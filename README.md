*Captain Unix oh-my-zsh theme*

To install the Captain Unix theme write on shell (the install replace your .zshrc)

```bash
git clone https://gitlab.com/captainUnix/oh-my-zsh.git
cd oh-my-zsh/THEME/
./install
```

To install without replace .zshrc:

```bash
git clone https://gitlab.com/captainUnix/oh-my-zsh.git
cd oh-my-zsh/THEME/
cp captainUnix.zsh-theme ~/.oh-my-zsh/themes/
```


